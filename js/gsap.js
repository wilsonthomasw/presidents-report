var controller = new ScrollMagic.Controller();

new ScrollMagic.Scene({
                    triggerElement: "#triggerIntroduction",
                    triggerHook: 0.9, // show, when scrolled 10% into view
                   // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 50 // move trigger to center of element
                })
                .setClassToggle("#revealIntroduction", "visible") // add class to reveal
                .addTo(controller);

new ScrollMagic.Scene({
                    triggerElement: "#triggerGoals",
                    triggerHook: 0.9, // show, when scrolled 10% into view
                   // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 50 // move trigger to center of element
                })
                .setClassToggle("#revealGoals", "visible") // add class to reveal
                .addTo(controller);

new ScrollMagic.Scene({
                    triggerElement: "#triggerAchievements",
                    triggerHook: 0.9, // show, when scrolled 10% into view
                    offset: 50 // move trigger to center of element
                })
                .setClassToggle("#revealAchievements", "visible") // add class to reveal
                .addTo(controller);

new ScrollMagic.Scene({
                    triggerElement: "#triggerTimelines",
                    triggerHook: 0.9, // show, when scrolled 10% into view
    // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 50 // move trigger to center of element
                })
                .setClassToggle("#revealTimelines", "visible") // add class to reveal
                .addTo(controller);

new ScrollMagic.Scene({
                    triggerElement: "#triggerTargetingStudents",
                    triggerHook: 0.9, // show, when scrolled 10% into view
                    offset: 50 // move trigger to center of element
                })
                .setClassToggle("#revealTargetingStudents", "visible") // add class to reveal
                .addTo(controller);

new ScrollMagic.Scene({
                    triggerElement: "#triggerEntryTerm",
                    triggerHook: 0.9, // show, when scrolled 10% into view
                    // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 50 // move trigger to center of element
                })
                .setClassToggle("#revealEntryTerm", "visible") // add class to reveal
                .addTo(controller);

new ScrollMagic.Scene({
                    triggerElement: "#triggerDemonstratedInterest",
                    triggerHook: 0.9, // show, when scrolled 10% into view
                    // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 50 // move trigger to center of element
                })
                .setClassToggle("#revealDemonstratedInterest", "visible") // add class to reveal
                .addTo(controller);

new ScrollMagic.Scene({
                    triggerElement: "#triggerEstPerformance",
                    triggerHook: 0.9, // show, when scrolled 10% into view
                    // hide 10% before exiting view (80% + 10% from bottom)
                    offset: 50 // move trigger to center of element
                })
                .setClassToggle("#revealEstPerformance", "visible") // add class to reveal
                .addTo(controller);
